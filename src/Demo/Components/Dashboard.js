import React from 'react';
import Clock from './Clock'

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
  }

  clockRender() {
    return this.props.locations.map((val) => {
      return (
        <Clock
          timezone={val.timezone}
          title={val.title}
          key={val.title}
        />
      )
    });
  }

  render() {
    return (
      <div>
        {this.clockRender()}
      </div>
    );
  }
}

export default Dashboard
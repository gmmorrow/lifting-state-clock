import React from 'react';
import Clock from './Components/Clock.js'
import './App.css';

function App() {
  return (
    <div className="App">
      <Clock />
    </div>
  );
}

export default App;

import React from 'react';
import Dashboard from './Components/'
import './App.css';

function App() {
  return (
    <div className="App">
      <Dashboard />
    </div>
  );
}

export default App;

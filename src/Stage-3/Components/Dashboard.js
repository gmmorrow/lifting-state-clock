import React from 'react';
import Clock from './Clock'

class Dashboard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      newYork: 'America/New_York',
      alaska: 'America/Anchorage',
      dubai: 'Asia/Dubai'
    };
  }


  render() {
    return (
      <div>
        <Clock 
          location="New York"
          timezone={this.state.newYork}
        />
        <Clock 
          location="Alaska"
          timezone={this.state.alaska}
        />
        <Clock 
          location="India"
          timezone={this.state.dubai}
        />
      </div>
    );
  }
}

export default Dashboard
import React from 'react';
import moment from 'moment';
import 'moment-timezone';

class Clock extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      newYork: moment.tz('America/New_York').format('MMMM Do YYYY, h:mm:ss a'),
      alaska: moment.tz('America/Anchorage').format('MMMM Do YYYY, h:mm:ss a'),
      dubai: moment.tz('Asia/Dubai').format('MMMM Do YYYY, h:mm:ss a')
    };
  }

  componentDidMount() {
    this.interval = setInterval(() => this.second(), 1000);
  }

  second() {
    this.setState({
      newYork: moment.tz('America/New_York').format('MMMM Do YYYY, h:mm:ss a'),
      alaska: moment.tz('America/Anchorage').format('MMMM Do YYYY, h:mm:ss a'),
      dubai: moment.tz('Asia/Dubai').format('MMMM Do YYYY, h:mm:ss a')
    });
  }

  render() {
    return (
      <div>
        <div>
          <h1>New York</h1>
          {this.state.newYork}
        </div>
        <div>
          <h1>Albania</h1>
          {this.state.alaska}
        </div>
        <div>
          <h1>India</h1>
          {this.state.dubai}
        </div>
      </div>
    );
  }
}

export default Clock